type Controls = { [key: string]: string };
type ControlsCombinations = { [key: string]: string[] };

export const controls: Controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
};

export const controlsCombinations: ControlsCombinations = {
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO'],
};
