import { controls, controlsCombinations } from '../../constants/controls';

export const formControlArray = (): string[] => {
  const allControls: string[] = [];
  Object.keys(controls).forEach((key) => allControls.push(controls[key]));
  Object.keys(controlsCombinations).forEach((key) => allControls.push(...controlsCombinations[key]));
  return allControls;
};

export const checkForHitCombo = (activeControls: Set<string>, combination: string[]): boolean => {
  return combination.reduce<boolean>((canHit, c) => {
    return canHit && activeControls.has(c);
  }, true);
};
