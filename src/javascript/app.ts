import { IFighter } from './interfaces/fighter';
import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement = document.getElementById('root') as HTMLElement;
  static loadingElement: HTMLElement = document.getElementById('loading-overlay') as HTMLElement;

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = (await fighterService.getFighters()) as IFighter[];
      const fightersElement = createFighters(fighters as IFighter[]);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
