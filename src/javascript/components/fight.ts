import { IFighter } from './../interfaces/fighter';
import { controls, controlsCombinations } from '../../constants/controls';
import { formControlArray, checkForHitCombo } from '../helpers/controlsHelper';

export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  return new Promise((resolve) => {
    const allControls = formControlArray();
    const controlsPressed: Set<string> = new Set();
    const timeToGainHitCombo: number = 10000;

    const firstHealthBar = document.getElementById('left-fighter-indicator') as HTMLElement;
    const secondHealthBar = document.getElementById('right-fighter-indicator') as HTMLElement;
    firstFighter.currentHealth = firstFighter.health;
    firstFighter.canHitCombo = true;
    secondFighter.currentHealth = secondFighter.health;
    secondFighter.canHitCombo = true;

    document.addEventListener('keydown', function onKeyDownPressed(event) {
      if (event.repeat) return;
      if (allControls.includes(event.code)) controlsPressed.add(event.code);

      //playerOne attack
      const mayPlayerOneAttack =
        controlsPressed.has(controls.PlayerOneAttack) &&
        !controlsPressed.has(controls.PlayerOneBlock) &&
        !controlsPressed.has(controls.PlayerTwoBlock);

      if (mayPlayerOneAttack) secondFighter = playerAttack(firstFighter, secondFighter, secondHealthBar);

      //playerTwo attack
      const mayPlayerTwoAttack =
        controlsPressed.has(controls.PlayerTwoAttack) &&
        !controlsPressed.has(controls.PlayerTwoBlock) &&
        !controlsPressed.has(controls.PlayerOneBlock);

      if (mayPlayerTwoAttack) firstFighter = playerAttack(secondFighter, firstFighter, firstHealthBar);

      //playerOne criticalHitCombination
      const mayPlayerOneUseHitCombo =
        checkForHitCombo(controlsPressed, controlsCombinations.PlayerOneCriticalHitCombination) &&
        (firstFighter.canHitCombo as boolean);

      if (mayPlayerOneUseHitCombo) {
        const [fA, fD] = playerDoCombo(firstFighter, secondFighter, secondHealthBar);
        firstFighter = fA;
        secondFighter = fD;
        setTimeout(() => {
          firstFighter.canHitCombo = true;
        }, timeToGainHitCombo);
      }

      //playerTwo criticalHitCombination
      const canPlayerTwoUseHitCombo =
        checkForHitCombo(controlsPressed, controlsCombinations.PlayerTwoCriticalHitCombination) &&
        (secondFighter.canHitCombo as boolean);

      if (canPlayerTwoUseHitCombo) {
        const [fA, fD] = playerDoCombo(secondFighter, firstFighter, firstHealthBar);
        secondFighter = fA;
        firstFighter = fD;
        setTimeout(() => {
          secondFighter.canHitCombo = true;
        }, timeToGainHitCombo);
      }

      if ((firstFighter.currentHealth as number) <= 0) resolve(secondFighter);
      else if ((secondFighter.currentHealth as number) <= 0) resolve(firstFighter);
    });

    document.addEventListener('keyup', function onKeyUpPressed(event) {
      if (allControls.includes(event.code)) controlsPressed.delete(event.code);
    });
  });
}

export function getDamage(attacker: IFighter, defender: IFighter): number {
  let damage: number;
  const attack = getHitPower(attacker);
  const defend = getBlockPower(defender);

  if (defend >= attack) damage = 0;
  else damage = attack - defend;

  return damage;
}

export function getHitPower(fighter: IFighter): number {
  const criticalHitChance = Math.random() + 1;
  const power = (fighter.attack as number) * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: IFighter): number {
  const dodgeChance = Math.random() + 1;
  const power = (fighter.defense as number) * dodgeChance;
  return power;
}

function playerAttack(attacker: IFighter, defender: IFighter, defenderHealthBar: HTMLElement): IFighter {
  const newCurrHealth = (defender.currentHealth as number) - getDamage(attacker, defender);
  defenderHealthBar.style.width = `${((newCurrHealth as number) / (defender.health as number)) * 100}%`;
  return {
    ...defender,
    currentHealth: newCurrHealth,
  };
}

function playerDoCombo(attacker: IFighter, defender: IFighter, defenderHealthBar: HTMLElement): Array<IFighter> {
  const newCurrHealth = (defender.currentHealth as number) - 2 * (attacker.attack as number);
  defenderHealthBar.style.width = `${((newCurrHealth as number) / (defender.health as number)) * 100}%`;
  return [
    {
      ...attacker,
      canHitCombo: false,
    },
    {
      ...defender,
      currentHealth: newCurrHealth,
    },
  ];
}
