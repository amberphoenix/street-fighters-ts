import { IFighter } from './../../interfaces/fighter';
import { showModal } from './modal';

export function showWinnerModal(fighter: IFighter): void {
  showModal({
    title: fighter.name,
    bodyElement: 'Goood job!',
    onClose: () => {
      document.location.reload();
    },
  });
}
