export interface IFighter {
  _id: string;
  name: string;
  health?: number;
  currentHealth?: number;
  attack?: number;
  canHitCombo?: boolean;
  defense?: number;
  source: string;
}
